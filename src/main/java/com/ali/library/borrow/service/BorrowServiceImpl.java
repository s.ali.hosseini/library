package com.ali.library.borrow.service;

import com.ali.library.book.persistance.BookRepository;
import com.ali.library.borrow.persistance.Borrow;
import com.ali.library.borrow.persistance.BorrowRepository;
import com.ali.library.member.persistance.MemberRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional
public class BorrowServiceImpl implements BorrowService {

     private BorrowRepository borrowRepository;
     private MemberRepository memberRepository;
     private BookRepository bookRepository;


     @Override
     public Borrow add(Borrow borrow) {
          if(bookRepository.findById(borrow.getBook().getId()).isEmpty())
               throw new BorrowException("book with id = " + borrow.getBook().getId() + " does not exist");
          if(memberRepository.findById(borrow.getMember().getId()).isEmpty())
               throw new BorrowException("member with id = " + borrow.getMember().getId() + " does not exist");
          List<Borrow> borrows = borrowRepository.findAllByMemberId(borrow.getMember().getId());

          if(borrows.stream().anyMatch(Borrow::isNeedToPayPenalty))

               throw new BorrowException("member with id = " + borrow.getMember().getId() + " has penalty that has not been payed yet");
          borrow.setLendDate(LocalDate.now());
          return borrowRepository.save(borrow);
     }

     @Override
     public Borrow getBorrow(Integer id) {
          return borrowRepository.findById(id).get();
     }

     @Override
     public List<Borrow> getAllBorrows() {
          return borrowRepository.findAll();
     }



     @Override
     public void delete(Integer id) {
          Borrow borrow = new Borrow();
          borrow.setId(id);
          borrowRepository.delete(borrow);
     }

     @Override
     public Borrow update(Borrow borrow) {
          return borrowRepository.save(borrow);
     }
}
