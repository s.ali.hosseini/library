package com.ali.library.borrow.service;


import com.ali.library.borrow.persistance.Borrow;

import java.util.List;

public interface BorrowService {

    Borrow add(Borrow borrow);
    Borrow getBorrow(Integer id);

    List<Borrow> getAllBorrows();
    void delete(Integer id);
    Borrow update(Borrow borrow);

}
