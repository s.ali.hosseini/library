package com.ali.library.borrow.controller.req;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BorrowCreateModel {

    private Integer memberId;
    private Integer bookId;
}
