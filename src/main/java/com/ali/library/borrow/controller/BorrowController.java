package com.ali.library.borrow.controller;

import com.ali.library.book.persistance.Book;
import com.ali.library.borrow.controller.req.BorrowCreateModel;
import com.ali.library.borrow.controller.req.BorrowUpdateModel;
import com.ali.library.borrow.controller.res.BorrowDetailedModel;
import com.ali.library.borrow.controller.res.BorrowSummaryModel;
import com.ali.library.borrow.persistance.Borrow;
import com.ali.library.borrow.service.BorrowService;
import com.ali.library.member.persistance.Member;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/borrow")
public class BorrowController {

    private BorrowService borrowService;

    @Autowired
    public BorrowController(BorrowService borrowService) {

        this.borrowService = borrowService;
    }




    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody BorrowSummaryModel add(@RequestBody BorrowCreateModel createModel) {
        Borrow borrow = new Borrow();
        borrow.setBook(Book.builder().id(createModel.getBookId()).build());
        borrow.setMember(Member.builder().id(createModel.getMemberId()).build());

        Borrow persistedBorrow = borrowService.add(borrow);

        BorrowSummaryModel borrowSummary = new BorrowSummaryModel();
        BeanUtils.copyProperties(persistedBorrow, borrowSummary);
        borrowSummary.setBookId(persistedBorrow.getBook().getId());
        borrowSummary.setMemberId(persistedBorrow.getMember().getId());

        return borrowSummary;



    }




    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody  BorrowDetailedModel get(@PathVariable Integer id) {
        Borrow borrow = borrowService.getBorrow(id);
        BorrowDetailedModel borrowDetailedModel = new BorrowDetailedModel();
        BeanUtils.copyProperties(borrow, borrowDetailedModel);

        borrowDetailedModel.setBookId(borrow.getBook().getId());  
        borrowDetailedModel.setMemberId(borrow.getMember().getId());
        return borrowDetailedModel;
    }






    @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody BorrowDetailedModel update(@RequestBody BorrowUpdateModel updateModel) {
        Borrow borrow = new Borrow();

        BeanUtils.copyProperties(updateModel, borrow);
        borrow.setBook(Book.builder().id(updateModel.getBookId()).build());
        borrow.setMember(Member.builder().id(updateModel.getMemberId()).build());

        Borrow updatedBorrow = borrowService.update(borrow);
        BorrowDetailedModel borrowDetail = new BorrowDetailedModel();
        BeanUtils.copyProperties(updatedBorrow, borrowDetail);
        borrowDetail.setBookId(updatedBorrow.getBook().getId());
        borrowDetail.setMemberId(updatedBorrow.getMember().getId());

        return borrowDetail;
    }




    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void delete(@PathVariable("id") Integer id) {

        borrowService.delete(id);

    }


}
