package com.ali.library.borrow.controller.res;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class BorrowDetailedModel {

    private Integer id;
    private LocalDate lendDate;
    private LocalDate returnDate;
    private boolean needToPayPenalty;
    private Integer memberId;
    private Integer bookId;
}
