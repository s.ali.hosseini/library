package com.ali.library.borrow.controller.res;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
//@Builder
@NoArgsConstructor
public class BorrowSummaryModel {


    private Integer id;
    private LocalDate lendDate;
    private Integer memberId;
    private Integer bookId;
}
