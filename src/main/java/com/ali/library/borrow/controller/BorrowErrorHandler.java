package com.ali.library.borrow.controller;

import com.ali.library.book.controller.res.ErrorResponse;
import com.ali.library.borrow.service.BorrowException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class BorrowErrorHandler {

    @ExceptionHandler(BorrowException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    ErrorResponse handleUserServiceExceptions(BorrowException ex, WebRequest request){
        ErrorResponse errorResponse = ErrorResponse
                .builder()
                .message(ex.getMessage())
                .path(request.getDescription(false))
                .timestamp(LocalDateTime.now())
                .build();
        return errorResponse;
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ErrorResponse handleAllExceptions(Exception ex, WebRequest request){
        ErrorResponse errorResponse = ErrorResponse
                .builder()
                .message(ex.getMessage())
                .path(request.getDescription(false))
                .timestamp(LocalDateTime.now())
                .build();
        return errorResponse;
    }





}
