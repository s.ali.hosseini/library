package com.ali.library.borrow.persistance;

import com.ali.library.base.RunConfiguration;
import com.ali.library.book.persistance.Book;
import com.ali.library.member.persistance.Member;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Borrow",schema = RunConfiguration.DB )
@Builder
public class Borrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "Return_Date")
    private LocalDate returnDate;

    @Column(name = "Lend_Date" , nullable = false)
    private LocalDate lendDate;

    @Column(name = "Need_To_Pay_Penalty")
    private boolean needToPayPenalty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Member_ID" , referencedColumnName = "id")
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Book_ID" , referencedColumnName = "id")
    private Book book;

}
