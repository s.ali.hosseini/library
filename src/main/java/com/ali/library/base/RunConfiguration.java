package com.ali.library.base;

import lombok.Data;

@Data
public class RunConfiguration {

    public static final String DB = "library";
}
