package com.ali.library.category.persistance;
import com.ali.library.base.RunConfiguration;
import com.ali.library.book.persistance.Book;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Category" ,schema = RunConfiguration.DB)
@Builder
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "Title")
    private String title;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private List<Book> bookList;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Parent_ID", referencedColumnName = "id")
    private Category ParentID;

    @OneToMany(mappedBy = "ParentID", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Category> categoryList;

}

