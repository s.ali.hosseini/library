package com.ali.library.member.controller;

import com.ali.library.member.controller.req.MemberCreateModel;
import com.ali.library.member.controller.req.MemberUpdateModel;
import com.ali.library.member.controller.res.MemberDetailedModel;
import com.ali.library.member.controller.res.MemberSummaryModel;
import com.ali.library.member.persistance.Member;
import com.ali.library.member.service.MemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/member")
public class MemberController {

private MemberService memberService;

    @Autowired
    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody MemberSummaryModel add(@RequestBody MemberCreateModel memberModel) {
        Member member = new Member();
        BeanUtils.copyProperties(memberModel, member);

        Member persistedMember = memberService.add(member);

        MemberSummaryModel memberSummary = new MemberSummaryModel();
        BeanUtils.copyProperties(persistedMember, memberSummary);

        return memberSummary;
    }

    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody MemberDetailedModel getMember(@PathVariable Integer id) {
        Member member = memberService.getMember(id);
        MemberDetailedModel memberDetailedModel = new MemberDetailedModel();
        BeanUtils.copyProperties(member, memberDetailedModel);
        return memberDetailedModel;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody List<MemberSummaryModel> getAll() {
        List<MemberSummaryModel> memberSummaryModels = memberService
                .getAllMembers()
                .stream()
                .map(member -> MemberSummaryModel
                        .builder()
                        .id(member.getId())
                        .nationalCode(member.getNationalCode())
                        .firstName(member.getFirstName())
                        .lastName(member.getLastName()).build())
                        .collect(Collectors.toList());
        return memberSummaryModels;
    }

  @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody MemberDetailedModel update(@RequestBody MemberUpdateModel memberModel) {
        Member member = new Member();
        BeanUtils.copyProperties(memberModel, member);
        Member updatedMember = memberService.update(member);
        MemberDetailedModel memberSummary = new MemberDetailedModel();
        BeanUtils.copyProperties(updatedMember, memberSummary);
        return memberSummary;
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void delete(@PathVariable("id") Integer id) {
        memberService.delete(id);
    }


}
