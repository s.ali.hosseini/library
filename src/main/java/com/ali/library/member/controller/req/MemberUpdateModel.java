package com.ali.library.member.controller.req;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MemberUpdateModel {

    private Integer id;
    private String nationalCode;
    private String firstName;
    private String lastName;
    private String gender;
    private String tell;
    private String address;

}
