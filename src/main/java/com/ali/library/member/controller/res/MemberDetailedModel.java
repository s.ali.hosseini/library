package com.ali.library.member.controller.res;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MemberDetailedModel {

    private Integer id;
    private String nationalCode;
    private String firstName;
    private String lastName;
    private String gender;
    private String tell;
    private String address;



}
