package com.ali.library.member.controller.res;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MemberSummaryModel {

    private Integer id;
    private String nationalCode;
    private String firstName;
    private String lastName;




}
