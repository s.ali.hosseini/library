package com.ali.library.member.service;

import com.ali.library.book.service.BookException;
import com.ali.library.member.persistance.Member;
import com.ali.library.member.persistance.MemberRepository;
import lombok.AllArgsConstructor;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional
public class MemberServiceImpl implements MemberService {

    private MemberRepository memberRepository;

    @Override
    public Member add(Member member) {

        try{
            return memberRepository.save(member);
        }catch (RuntimeException ex){
            if (ex.getCause() instanceof ConstraintViolationException)
                throw new MemberException("national-code should be unique");
            else if (ex.getCause() instanceof PropertyValueException)
                throw new MemberException("som property are null");
                else
                    throw ex;
        }

    }

    @Override
    public Member getMember(Integer id) {

         Optional<Member> memberOptional = memberRepository.findById(id);
        if (memberOptional.isEmpty())
            throw new BookException("sorry!!! member with id = " + id + " does not exist");
        return memberOptional.get();

    }

    @Override
    public List<Member> getAllMembers() {

        return memberRepository.findAll();
    }

    @Override
    public void delete(Integer id) {

        memberRepository.delete(Member.builder().id(id).build());

    }

    @Override
    public Member update(Member member) {

        Optional<Member> memberOptional= memberRepository.findById(member.getId());
        if (memberOptional.isEmpty())
            throw new MemberException("member does not exist");
        Member notUpdateMember = memberOptional.get();
        BeanUtils.copyProperties(member, notUpdateMember);
        return notUpdateMember;

    }



}
