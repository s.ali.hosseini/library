package com.ali.library.member.service;

import com.ali.library.member.persistance.Member;

import java.util.List;

public interface MemberService {


    Member add(Member member);

    Member getMember(Integer id);

    List<Member> getAllMembers();

    void delete(Integer id);

    Member update(Member member);




}
