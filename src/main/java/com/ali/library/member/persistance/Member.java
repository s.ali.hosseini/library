package com.ali.library.member.persistance;
import com.ali.library.base.RunConfiguration;
import com.ali.library.borrow.persistance.Borrow;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Member" ,schema = RunConfiguration.DB)
@Builder
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "National_Code" , nullable = false, unique = true)
    private String nationalCode;
    @Column(name = "First_Name" , nullable = false)
    private String firstName;

    @Column(name = "Last_Name", nullable = false)
    private String lastName;

    @Column(name = "Address")
    private String address;

    @Column(name = "Phone_Number", nullable = false)
    private String tell;

    @Column(name = "Gender", nullable = false)
    private String gender;


    @OneToMany(mappedBy = "member",fetch = FetchType.LAZY)
    Collection<Borrow> borrowList;



}
