package com.ali.library.book.service;

import com.ali.library.book.persistance.Book;
import com.ali.library.book.persistance.BookRepository;
import lombok.AllArgsConstructor;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional
public class BookServiceImpl implements BookService {


    private  BookRepository bookRepository;


    @Override
    public Book add(Book book) {
        try {
            return bookRepository.save(book);
        }catch (RuntimeException ex){
            if(ex.getCause() instanceof ConstraintViolationException)
                throw new BookException("isbn should be unique");
            else if (ex.getCause() instanceof PropertyValueException)
                throw new BookException("some properties are null");
            else
                throw ex;
        }
    }

    @Override
    public Book getBook(Integer id) {
        Optional<Book> bookOptional = bookRepository.findById(id);
        if (bookOptional.isEmpty())
            throw new BookException("book with id = " + id + " does not exist");
        return bookOptional.get();
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        bookRepository.delete(Book.builder().id(id).build());
    }

    @Override
    public Book update(Book book) {
        Optional<Book> bookOptional = bookRepository.findById(book.getId());
        if (bookOptional.isEmpty())
            throw new BookException("book does not exist");
        Book notUpdateBook = bookOptional.get();
        BeanUtils.copyProperties(book, notUpdateBook);
        return notUpdateBook;
    }
}
