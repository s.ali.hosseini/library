package com.ali.library.book.service;

import com.ali.library.book.persistance.Book;

import java.util.List;

public interface BookService {

Book add(Book book);

Book getBook(Integer id);

List<Book> getAllBooks();

void delete(Integer id);

Book update(Book book);


}
