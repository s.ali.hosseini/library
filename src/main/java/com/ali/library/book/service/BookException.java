package com.ali.library.book.service;

public class BookException extends RuntimeException {
    public BookException(String message) {
        super(message);
    }
}
