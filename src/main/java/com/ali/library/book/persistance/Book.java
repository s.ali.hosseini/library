package com.ali.library.book.persistance;
import com.ali.library.base.RunConfiguration;
import com.ali.library.borrow.persistance.Borrow;
import com.ali.library.category.persistance.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Book" ,schema = RunConfiguration.DB)
@Builder
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "Title" , nullable = false)
    private String title;
    @Column(name = "Author" , nullable = false)
    private String author;
    @Column(name = "Publish_Year")
    private String publishYear;
    @Column(name = "ISBN" , nullable = false, unique = true)
    private String isbn;
    @Column(name = "Price" , nullable = false)
    private Integer price;
    @Column(name = "Quantity" , nullable = false)
    private Integer quantity;

    @OneToMany(mappedBy = "book",fetch = FetchType.LAZY)
    Collection<Borrow> borrowList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id",referencedColumnName ="id")
    private Category category;

}