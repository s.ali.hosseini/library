package com.ali.library.book.controller;

import com.ali.library.book.controller.res.BookDetailedModel;
import com.ali.library.book.controller.res.BookSummaryModel;
import com.ali.library.book.controller.req.BookCreateModel;
import com.ali.library.book.controller.req.BookUpdateModel;
import com.ali.library.book.persistance.Book;
import com.ali.library.book.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/book")
public class BookController {


    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody BookSummaryModel add(@RequestBody BookCreateModel bookModel) {

        Book book = new Book();
        BeanUtils.copyProperties(bookModel, book);

        Book persistedBook = bookService.add(book);
        BookSummaryModel bookSummary = new BookSummaryModel();
        BeanUtils.copyProperties(persistedBook, bookSummary);

        return bookSummary;
    }


    @GetMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody BookDetailedModel getBook(@PathVariable Integer id) {
        Book book = bookService.getBook(id);
        BookDetailedModel bookDetailedModel = new BookDetailedModel();
        BeanUtils.copyProperties(book, bookDetailedModel);
        return bookDetailedModel;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<BookSummaryModel> getAll() {
        List<BookSummaryModel> bookSummaryModels = bookService
                .getAllBooks()
                .stream()
                .map(book -> BookSummaryModel
                        .builder()
                        .id(book.getId())
                        .isbn(book.getIsbn())
                        .title(book.getTitle()).build())
                .collect(Collectors.toList());
        return bookSummaryModels;
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody BookDetailedModel update(@RequestBody BookUpdateModel bookModel) {
        Book book = new Book();
        BeanUtils.copyProperties(bookModel, book);
        Book updatedBook = bookService.update(book);
        BookDetailedModel bookSummary = new BookDetailedModel();
        BeanUtils.copyProperties(updatedBook, bookSummary);
        return bookSummary;
    }



    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void delete(@PathVariable("id") Integer id) {
        bookService.delete(id);
    }
}