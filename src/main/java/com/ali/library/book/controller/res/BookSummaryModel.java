package com.ali.library.book.controller.res;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookSummaryModel {

    private Integer id;
    private String title;
    private String isbn;


}
