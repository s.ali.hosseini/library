package com.ali.library.book.controller.res;
import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;


@Builder
@Data
public class ErrorResponse {

    private final LocalDateTime timestamp;
    private final String path;
    private final String message;

}
