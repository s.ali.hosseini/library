package com.ali.library.book.controller.res;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookDetailedModel {

    private Integer id;
    private Integer price;
    private Integer quantity;
    private String title;
    private String author;
    private String publishYear;
    private String isbn;
}
