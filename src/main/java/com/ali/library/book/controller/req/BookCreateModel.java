package com.ali.library.book.controller.req;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookCreateModel {
    private Integer price;
    private Integer quantity;
    private String title;
    private String author;
    private String publishYear;
    private String isbn;
}