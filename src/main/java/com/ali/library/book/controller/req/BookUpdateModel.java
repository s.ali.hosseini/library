package com.ali.library.book.controller.req;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookUpdateModel {

    private Integer id;
    private Integer price;
    private Integer quantity;
    private String title;
    private String author;
    private String publishYear;
    private String isbn;
}
